//Comments

//Comments are section in the codethat is not read by the language
//It isfor the user's to know what the code does

//In JavaScript, there are two types of comments:

/*
	1. The single line comments, denoted by //
	2. The multi line comment, denoted by slash and as asterisk
*/

//alert("Hello World");

/* Syntax and Statements

Syntax contains the rules needed to create a statement in a programming language
Statements are instruction we tell the applicatiob to perform.

Statements end with a semicolon(;) as best practice

*/

/*Variables and constants are containers for data that can be used to store information and retrieve or manipulate in the future

Variables can contain values that can change as the program runs
Constants contain values that cannot change as the program runs

To create a variable, we use the "let" keyword (keyword are special words in a programming language)

To create a constant, we use the "const" keyword

*/


let productName = "Desktop computer"; // "Desktop computer"is a string usually have " " or ' ' to indicate they are strings
let productPrice = 18999; //18999 is a number, number do not need "" or '' to indicate they are numbers
const PI = 3.1416; //PI is constant because value of PI can never be change

/*

Rules in naming variables and constants:
1. variable names start with lowercase letters, for multiple words, we use camelCase
2. variable names should be descriptive of what values it can contain

*/

/*
Console - is a part of a browser wherein outputs can be displayed. It can be accessed via the console tab in any browser

To outout a value in the console, we use the console.log function

*/

console.log(productName); //console is an object in JS and .log is a function that allows writing of the output
console.log(productPrice);
console.log(PI);

console.log("hello world"); //We can directly output any value
console.log(12345);
console.log("I am selling " + productName);


/*
Data types - are type of data a variable can handle
1. String - a combination of alphanumeric values (ex. Sharmane, password123)
2. Number - positive, negative,decimal values (ex. 100, -9, 3.1419)
3. Boolean - truth values (ex. true, false)
4. BigInt* (not used often)
5. Symbol* (not used often)
6. Undefined- only appears if a variable was not assigned any value
7. Null - the variable is intentionally left to have a null value
8. Object - combination of data with key-values

*/

// String
let fullName = "Brandon B. Brandon";
let schoolName = "Zuitt Coding Bootcamp";
let userName = "brandon00";

//Number
let age = 28;
let numberOfPets = 5;
let desiredGrade = 98.5;

//Boolean
let isSingle = true; //No need for " " if Boolean
let hasEaten = false;

//Undefined
let petName; //undefined because we did not assign any value, wala lang nabigay na value

//Null
let grandChildName = null; //you don't have any grandchildren yet, sinadyang wala

//Object
let person = {
	firstName : "Jobert", //the comma indicates there are still more properties after this line
	lastName : "Boyd",
	age : 15,
	petName : "Whitey"//you don't need comma for the last property
}; //semicolon is after the curly brace


/*
Operators - allow us to perform operations or evaluate results
There are 5 main types of operators:
1. Assignment
2. Arithmetic
3. Comparison
4. Relational
5. Logical

*/

//Assignment operator uses the = symbol -> it assigns a value to a variable

let num1 = 28;
let num2 = 75;

//Arithmetic operator has 5 operations
let sum = num1 + num2;
console.log(sum);
let difference = num1 - num2;
console.log(difference);
let product = num1 * num2;
console.log(product);
let quotient = num2 / num1;
console.log(quotient);
let remainder = num2 % num1;

console.log(remainder);

//you can actually combine the assignment and arithmetic operators
let num3 = 17;
//num3 = num3 - 4;
//console.log(num3);
//The line num3 = num3 -4 can be reduced into this line:
num3 -= 4; //this is the same as num3 = num3 - 4; this is shorthand version

//shorthand version?
// x = x / y -> x /= y

//Special arithmetic ++ and -- (increment and decrement +1 or -1)

let num4 = 5;
num4++;
console.log(num4);
//find out difference of num++ and ++num

//Comparison and Relational
//Comparison compares two alues if they are equal or not
// ==, ===, !=, !==
let numA = 65;
let numB = 65;
console.log(numA == numB);

let statement1 = "true";
let statement2 = true;

console.log(statement1 == statement2);
console.log(statement1 === statement2);
//sir T to revert back to the students


//Relational compares two numbers if they are equal or not
// >, <, >=, <=
let numC = 25;
let numD = 45;

console.log(numC > numD); //false
console.log(numC < numD); //true
console.log(numC >= numD); //false

//Logical - compares two boolean value
//AND (&&), OR (||), NOT (!)
let isTall = false;
let isDark = true;
let isHandsome = true;
let didPAssStandard = isTall && isDark; //both conditions should be true
console.log(didPAssStandard); 

let didPassLowerStandard = isTall || isDark; //needs only at least one condition to be true
console.log(didPassLowerStandard); //true

console.log(!isTall); //true -> !reverses the value 
//Note: you can combine comparison,logical and relational
let result = isTall || (isDark && isHandsome);
console.log(result); //true

/*

Functions - are groups of statements that perform a single action to prevent code duplication
A function has two main concepts
1. Function declaration (definition) -defines what the function does
2. Function invocation (calling) - using the function

*/

//Function declaration

function createFullName(fName, mName, lName){
	return fName + mName + lName;
}

/*
function is a keyword in JS that says that it is a function createFullName is the name of the function, it should be descriptive of what it does fName, mName, lname are called parameters, these are the inputs that the function needs in order to work. Note: parameters are optional, meaning you can create a function without any parameters
fName, mName, lName are placeholders for values and is only used in the function

return is a keyword in JS that returns the resulting value after the function is completely run. Note: this is also optional if you don't want any returning values.
*/

//Function invocation
/*let fullName1 = createFullName("Brandon","Ray","Smith");
console.log(fullName); //BrandonRaySmith
let fullName2 = createFullName("John", "Robert", "Smith");
console.log(fullName2); //JohnRobertSmith
let fN = "Jobert";
let mN = "Bob";
let lN = "Garcia";
letFullName3 = createFullName(fN, mN, lN),
console.log(fullName3);
let fullName4 = createFullName(mN, fN, lN);
console.log(fullName4);*/


//declaration
function addTwo(firstNum, secondNum){
	return 2*(firstNum + secondNum);
}

//to call the function
console.log(addTwo(1, 2));


